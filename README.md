Data driven design of Aluminium alloys
======================================

  * 63 elements (set it in `00_setup.sh`
  * *ab initio* calculation with automatic results parsing
  * Vinet fit is saved in a format ready for visualisation


Python scripts require python2.7, click, scipy and ase

**BEFORE RUNNING** add all necessary `POTCAR`s in `_infiles`, including the one for Al. Also check `INCAR` and `KPOINTS`.

Example directory structure
===========================

Assuming tree from the current directory, the scripts create `CALCS` subfolder for all calculations:

    *.sh
    CALCS/
      AlGa/
        eos/
          10/
            INCAR
            KPOINTS
            POSCAR
            POTCAR
          20/
            ...
          30/
            ...
          40/
            ...
          50/
            ...
          60/
            ...
          70/
            ...
        elastic/
          reference_d=0.00/
            INCAR
            KPOINTS
            POSCAR
            POTCAR
          monoclinic_d=-0.04/
            INCAR
            KPOINTS
            POSCAR
            POTCAR
          monoclinic_d=-0.02/
            ...
          monoclinic_d=-0.01/
            ...
          monoclinic_d=0.01/
            ...
          monoclinic_d=0.02/
            ...
          monoclinic_d=0.04/
            ...
          orthorombic_d=-0.04/
            INCAR
            KPOINTS
            POSCAR
            POTCAR
          orthorombic_d=-0.02/
            ...
          orthorombic_d=-0.01/
            ...
          orthorombic_d=0.01/
            ...
          orthorombic_d=0.02/
            ...
          orthorombic_d=0.04/
            ...
      AlLi/
        eos/
          ...
        elastic/
          ...
