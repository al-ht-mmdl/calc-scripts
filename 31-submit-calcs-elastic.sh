#!/bin/bash

source ./00-setup.sh

TOPDIR=$(pwd)

function drop_cherry_slurm() {
    ## Helper function to create a cherry.slurm file, call with two arguments:
    ## drop_cherry_slurm PATH_WHERE_TO_CREATE JOBNAME_TO_USE

    cat > ${1}/cherry.slurm <<EOF
#!/bin/bash
#SBATCH -N 4
#SBATCH -t 24:00:00
#SBATCH -J ${2}
#SBATCH --propagate=STACK

module load soft/vasp/5.3-intel
mpirun.sh ~/vasp_builds/vasp.5.3-02/vasp
EOF

}


for elem in ${elements_all}
do
    echo "=>> Submitting elastic constants calculations for Al${elem}/"

    # Make reference directory
    location=CALCS/Al${elem}/elastic/reference_d\=0.00
    drop_cherry_slurm ${location} "ec-Al${elem}:0.00"
    # submit reference directory
    cd ${location}
    sbatch cherry.slurm
    cd ${TOPDIR}

    # Make deformed cells
    for deform_type in monoclinic orthorombic
    do
        for d in -0.04 -0.02 -0.01 0.01 0.02 0.04
        do
            location=CALCS/Al${elem}/elastic/${deform_type}_d\=${d}
            drop_cherry_slurm  ${location} "ec-Al${elem}:${d}"

            cd ${location}
            sbatch cherry.slurm
            ls -l cherry.slurm
            cd ${TOPDIR}
        done
    done

done
