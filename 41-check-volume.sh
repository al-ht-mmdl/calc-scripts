#!/bin/bash

INPUT=2ND_STEP.data


for struct in AlGd AlAl #AlZr
do
        COUNT=$(cat ${INPUT} | awk '{print $1 " " $4}' | uniq | grep $struct -c)

        if [ ${COUNT} -ne 1 ]
        then
                echo "$struct: ERROR (volume mismatch)"
        else
                echo "$struct: OK"
        fi
done
