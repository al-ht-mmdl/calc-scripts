#!/bin/bash

source ./00-setup.sh

# TODO/IDEAS:
# - проверить, есть ли папка, может тогда её не надо создавать

for element in ${elements_all}
do
    n=CALCS/Al${element}
    echo "Creating input files for Al-${element}"

    mkdir $n
    mkdir $n/eos

    for index in 10 20 30 40 50 60 70
    do
        calcdir=$n/eos/${index}
        mkdir $calcdir

        cp _infiles/INCAR   $calcdir
        cp _infiles/KPOINTS $calcdir

        sed "s/TEMPLATE_LATPAR/${latpar[${index}]}/" \
            _infiles/POSCAR > $calcdir/POSCAR

        cat ${potcar_Al} ${potcars_path}/POTCAR_${element} > ${calcdir}/POTCAR
    done
done
