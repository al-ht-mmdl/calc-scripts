#!/bin/bash


source ./00-setup.sh

for precipitate in ${elements_all}
do
    struct=Al${precipitate}
    for deform_type in monoclinic orthorombic
    do
        for d in -0.04 -0.02 -0.01 0.01 0.02 0.04
        do
            outcar_file=CALCS/${struct}/elastic/${deform_type}_d\=${d}/OUTCAR
            ENERGY=$(bzgrep 'free  energy' ${outcar_file} | tail -n1 | awk '{print $5}')
            VOLUME=$(bzgrep 'volume of cell' ${outcar_file} | tail -n1 | awk '{print $5}')
            echo $struct $d $ENERGY $VOLUME $deform_type
        done
    done

    outcar_file=CALCS/${struct}/elastic/reference_d\=0.00/OUTCAR
    ENERGY=$(bzgrep 'free  energy' ${outcar_file} | tail -n1 | awk '{print $5}')
    VOLUME=$(bzgrep 'volume of cell' ${outcar_file} | tail -n1 | awk '{print $5}')
    echo $struct 0.00 $ENERGY $VOLUME reference
done
