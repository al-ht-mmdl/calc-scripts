#!/bin/bash

source ./00-setup.sh

TOPDIR=$(pwd)

function drop_cherry_slurm() {
    ## Helper function to create a cherry.slurm file, call with two arguments:
    ## drop_cherry_slurm PATH_WHERE_TO_CREATE JOBNAME_TO_USE

    cat > ${1}/cherry.slurm <<EOF
#!/bin/bash
#SBATCH -N 4
#SBATCH -t 24:00:00
#SBATCH -J ${2}
#SBATCH --propagate=STACK

module load soft/vasp/5.3-intel
mpirun.sh ~/vasp_builds/vasp.5.3-02/vasp
EOF

}


for el in ${elements_all}
do
    for index in 10 20 30 40 50 60 70
    do
        calc_dir=CALCS/Al${el}/eos/${index}

        drop_cherry_slurm  ${calc_dir} "10-${el}-$index"

        cd ${calc_dir}
        ls cherry.slurm
        sbatch cherry.slurm
        cd ${TOPDIR}
    done
done
