#!/bin/bash

source ./00-setup.sh

for e in Al ${elements_all}
do
    dir=CALCS/Al${e}
        for i in 10 20 30 40 50 60 70
        do

                tag=$(printf "%02d" $i)

                outcar_file=${dir}/eos/${tag}/OUTCAR
                kpoints_file=${dir}/eos/${tag}/KPOINTS

                encut=$(grep ENCUT ${outcar_file} | awk '{print $3}')

                kp=$(tail -n+4 ${kpoints_file} | head -n1)
                F=$(grep 'free  energy' ${outcar_file} | tail -n1 | awk '{print $5}')
                V=$(grep 'volume of cell' ${outcar_file} | tail -n1 | awk '{print $NF}')

                echo Al${e} $encut $kp $F $V
        done
done
