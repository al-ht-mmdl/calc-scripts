#!/bin/bash

elements_all="Ag As At Au B Ba Be Bi Br C Ca Cd Cl Co Cr Cs Cu F Fe Ga Ge Hf Hg I In Ir K Li Mg Mn Mo N Na Nb Ni O Os P Pb Pd Po Pt Rb Re Rh Ru S Sb Sc Se Si Sn Sr Ta Tc Te Ti Tl V W Y Zn Zr"

elements_10="Ag Au C Ca Cl Ga Li N S Ti"
elements_all_minus10="As At B Ba Be Bi Br Cd Co Cr Cs Cu F Fe Ge Hf Hg I In Ir K Mg Mn Mo Na Nb Ni O Os P Pb Pd Po Pt Rb Re Rh Ru Sb Sc Se Si Sn Sr Ta Tc Te Tl V W Y Zn Zr"

# Параметры решётки согласованы с Катей 2017/jun/13
latpar[10]=3.92000
latpar[20]=3.96000
latpar[30]=4.00000
latpar[40]=4.04000
latpar[50]=4.08000
latpar[60]=4.12000
latpar[70]=4.16000

# Это путь к поткарам
potcar_Al=_infiles/POTCAR_Al
potcars_path=_infiles/potcar_all

