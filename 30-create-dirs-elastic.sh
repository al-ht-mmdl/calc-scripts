#!/bin/bash

source ./00-setup.sh

FITDATA="21-fit-1st-step-data.tsv"

function give_me_vinet_latpar_for() {
    # Find the line for this element, return third column and transform volume
    # to latpar
    eq_lat=$(grep "$1 Vinet" $FITDATA | awk '{print $3}' \
        | python -c "v=float(raw_input()); print (v/27.0)**(1.0/3.0)")
    echo $eq_lat
}

function drop_ec_input_files_for() {
    # just copies everything to the calculation folder.
    # Aborts if the target directory exists already
    #
    # requires three parameters:
    #   first -- impurity element
    #  second -- deformation type
    #   third -- deform_value

    elem=$1
    deform_type=$2
    deform_value=$3

    location=CALCS/Al${elem}/elastic/${deform_type}_d\=${deform_value}
    if [ ! -d ${location} ]
    then
        mkdir --verbose --parents $location
        lat_par=$(give_me_vinet_latpar_for Al$elem)

        cp _infiles/INCAR   $location
        cp _infiles/KPOINTS $location
        cat ${potcar_Al} ${potcars_path}/POTCAR_${elem} > ${location}/POTCAR
        sed -e "s/__LATPAR__/${lat_par}/" \
            -e "s/__IMPURITY__/${elem}/" \
            -e "s/__STRUCT__/Al${elem}/" \
            _infiles/elastic_poscars/template_${deform_type}_d\=${deform_value} > ${location}/POSCAR
    else
        echo "  Existing directory for Al$elem (deformation: ${deform_type}, d=${deform_value}) left untouched."
    fi
}

for elem in ${elements_all} #${elements_10}
do
    echo "=>> Creating input files for elastic constants calc in Al${elem}/"

    # Make reference directory
    drop_ec_input_files_for $elem reference "0.00"

    # Make deformed cells
    for deform_type in monoclinic orthorombic
    do
        for d in -0.04 -0.02 -0.01 0.01 0.02 0.04
        do
            drop_ec_input_files_for $elem ${deform_type} "${d}"
        done
    done
done
