#!/usr/bin/gnuplot -persist

set term pngcairo
set output "AlZr.png"

set key box opaque

set xlabel "Volume" 
set ylabel "Energy" 

plot \
    'zzz.data' u 1:2 smooth csplines notitle,\
    'zzz.data' u 1:2 with points ps 3 pt 7 title "AlZr"
