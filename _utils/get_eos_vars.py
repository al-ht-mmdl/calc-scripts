#!/bin/env python2.7
# coding: utf-8

import click
import numpy as np
from datetime import datetime

from ase.test import NotAvailable
try:
    from ase.units import kJ
except ImportError:
    _e = 1.60217733e-19
    kJ = 1000.0 / _e

try:
    from scipy.optimize import curve_fit
except ImportError:
    raise NotAvailable('This script needs correctly installed scipy module to work.')
    from scipy.optimize import leastsq

    # this part comes from
    # http://projects.scipy.org/scipy/browser/trunk/scipy/optimize/minpack.py
    def _general_function(params, xdata, ydata, function):
        return function(xdata, *params) - ydata
    # end of this part

    def curve_fit(f, x, y, p0):
        func = _general_function
        args = (x, y, f)
        # this part comes from
        # http://projects.scipy.org/scipy/browser/trunk/scipy/optimize/minpack.py
        popt, pcov, infodict, mesg, ier = leastsq(func, p0, args=args, full_output=1)

        if ier not in [1, 2, 3, 4]:
            raise RuntimeError, "Optimal parameters not found: " + mesg
        # end of this part
        return popt, pcov


class EOS(object):
    """ Generic function for eos fitting. You need to subclass this and redefine
    fitting functions. Dump_fit will give you a range of points fitted using
    results from your data
    """
    EOS_NAME = 'Generic EOS'

    def __init__(self, volumes, energies):
        self.v = np.array(volumes)
        self.e = np.array(energies)

        self.v0 = None

    @classmethod
    def parabola(cls, x, a, b, c):
        '''
        parabola polynomial function

        this function is used to fit the data to get good guesses for
        the equation of state fits

        a 4th order polynomial fit to get good guesses for
        was not a good idea because for noisy data the fit is too wiggly
        2nd order seems to be sufficient, and guarentees a single minimum'''

        return a + b*x + c*x**2

    @classmethod
    def energy_fit_function(cls, V, E0, B0, BP, V0):
        pass

    @classmethod
    def pressure_fit_function(cls, V, E0, B0, BP, V0):
        pass

    def fit(self):
        """Calculate volume, energy, and bulk modulus.
        old ASE2 implementation

        Returns the optimal volume, the minumum energy, and the bulk
        modulus.  Notice that the ASE units for the bulk modulus is
        eV/Angstrom^3 - to get the value in GPa, do this::

          v0, e0, B, BP = eos.fit()
          print B / kJ * 1.0e24, 'GPa'

        """

        p0 = [min(self.e), 1, 1]
        popt, pcov = curve_fit(self.parabola, self.v, self.e, p0)

        parabola_parameters = popt
        # Make sure the minimum is bracketed by the volumes
        # (this if for the solver)
        minvol = min(self.v)
        maxvol = max(self.v)

        # the minimum of the parabola is at dE/dV = 0, or 2*c V +b =0
        c = parabola_parameters[2]
        b = parabola_parameters[1]
        a = parabola_parameters[0]
        parabola_vmin = -b/2/c

        if not (minvol < parabola_vmin and parabola_vmin < maxvol):
            print 'Warning the minimum volume of a fitted parabola is not in your volumes. You may not have a minimum in your dataset'

        # evaluate the parabola at the minimum to estimate the groundstate energy
        E0 = self.parabola(parabola_vmin, a, b, c)
        # estimate the bulk modulus from Vo*E''.  E'' = 2*c
        B0 = 2*c*parabola_vmin

        BP = 4 # just a guess

        initial_guess = [E0, B0, BP, parabola_vmin]

        # now fit the equation of state
        p0 = initial_guess
        popt, pcov = curve_fit(self.energy_fit_function, self.v, self.e, p0)

        self.eos_parameters = popt

        self.v0 = self.eos_parameters[3]
        self.e0 = self.eos_parameters[0]
        self.B = self.eos_parameters[1]
        self.BP = self.eos_parameters[2]

        return self.v0, self.e0, self.B, self.BP

    def dump_fit(self, filename):
        """ Output table of volumes into file, energies and pressures to stdout.
        Uses the volumes provided for fitting +- 10%

        """
        if self.v0 is None:
            self.fit()

        vol_list = np.linspace(min(self.v)*0.95, max(self.v)*1.1)
        vol_list = np.sort(np.append(vol_list, self.v))

        def pressure(v):
            return self.pressure_fit_function(
                v,
                self.eos_parameters[0],
                self.eos_parameters[1],
                self.eos_parameters[2],
                self.eos_parameters[3],
            )

        def energy(v):
            return self.energy_fit_function(
                v,
                self.eos_parameters[0],
                self.eos_parameters[1],
                self.eos_parameters[2],
                self.eos_parameters[3],
            )

        with open(filename, 'w') as fp:
            now_str = datetime.now().strftime("%Y-%b-%d %H:%M")
            print >>fp, "# %s EOS fit %s" % (self.EOS_NAME, now_str)
            print >>fp, "# V[Ang^3] Energy[eV] P[GPa]"
            for v, e, p in zip(vol_list, energy(vol_list), pressure(vol_list)):
                print >>fp, v, e, p / kJ * 1e24


class BirchMurnaghanEOS(EOS):
    """ Fit equation of state for bulk systems.
    Based on EquationOfStateASE2, but adds BP and fixed to Birch-Murnaghan

       birchmurnaghan
           as seen in PRB 70, 224107
           Original paper by Birch: DOI: 10.1103/PhysRev.71.809

    Use::

       eos = BirchMurnaghanEOS(volumes, energies)
       v0, e0, B, BP = eos.fit()

    """
    EOS_NAME = 'Birch-Murnaghan'

    @classmethod
    def energy_fit_function(cls, V, E0, B0, BP, V0):
        'BirchMurnaghan equation from PRB 70, 224107'

        eta = (V0/V)**(2./3.)
        E = E0 + 9.*B0*V0/16.*(eta-1)**2*(6 + BP*(eta-1.) - 4.*eta)
        return E

    @classmethod
    def pressure_fit_function(cls, V, E0, B0, BP, V0):
        'B-M pressure as a function of Volume using supplied parameters'

        eta = (V0/V)**(1./3.)
        P = 3.*B0 / 2. \
            * (eta**7 - eta**5) * (1. + 3.*(BP - 4.)/4. * (eta**2 - 1))
        return P


class VinetEOS(EOS):
    """ Fit equation of state for bulk systems.
    Based on EquationOfStateASE2, but adds BP and fixed to vinet

       vinet
           PRB 70, 224107

    Use::

       eos = VinetEOS(volumes, energies)
       v0, e0, B, BP = eos.fit()

    """
    EOS_NAME = 'Vinet'

    @classmethod
    def energy_fit_function(cls, V, E0, B0, BP, V0):
        'Vinet equation from PRB 70, 224107'

        eta = (V/V0)**(1./3.)

        E = (E0 + 2.*B0*V0/(BP-1.)**2 * (
            2. - (5. + 3.*BP*(eta-1.)-3.*eta)*np.exp(-3.*(BP-1.)*(eta-1.)/2.)
        ))
        return E

    @classmethod
    def pressure_fit_function(cls, V, E0, B0, BP, V0):
        'Vinet pressure as a function of Volume using supplied parameters'

        eta = (V/V0)**(1./3.)
        P = 3.*B0 * (1. - eta)/(eta**2) * np.exp(3./2.*(BP - 1.)*(1. - eta))
        return P


@click.command()
@click.option('--datafile', prompt="Please provide datafile name", help="datafile name")
@click.option('--prepare-for-js', is_flag=True, help="Format output ready for JS")
@click.option('--element-name', help="Element name if --prepare-for-js is on", default=None)
def main(datafile, prepare_for_js, element_name):
    """ Reads lines from `datafile`, lines expected to be in format

    volume/cell energy/cell

    You need at least a few points around minimum to make the fit trustworthy

    """

    d = []
    with open(datafile, 'r') as fp:
        for line in fp:
            if not line.startswith('#'):
                s = line.split()
                d += [(float(s[0]), float(s[1]))]

    eos = VinetEOS([v for v, _ in d], [e for _, e in d])
    V0, E0, B, BP = eos.fit()
    B = B / kJ * 1.0e24
    if prepare_for_js:
        print """%s: { // vinet
    'V0': %s,
    'E0': %s,
    'B': %s,
    'BP': %s
} // %s""" % (element_name, V0, E0, B, BP, element_name)
    else:
        print "Vinet %s %s %s %s" % (V0, E0, B, BP)

    eos = BirchMurnaghanEOS([v for v, _ in d], [e for _, e in d])
    V0, E0, B, BP = eos.fit()
    B = B / kJ * 1.0e24
    if prepare_for_js:
        print """%s: { // birch-murnaghan
  'V0': %s,
  'E0': %s,
  'B': %s,
  'BP': %s
} // %s""" % (element_name, V0, E0, B, BP, element_name)
    else:
        print "B.-M. %s %s %s %s" % (V0, E0, B, BP)


if __name__ == "__main__":
    main()
