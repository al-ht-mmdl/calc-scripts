#!/bin/bash

INPUT="20-1st-step-data.tsv"
OUTPUT_TSV="21-fit-1st-step-data.tsv"
OUTPUT_JSON="21-fit-1st-step-data.json"

if [ ! -f ${INPUT} ]
then
    echo "Cannot find '${INPUT}'. Aborting"
    exit
fi

if [ -f ${OUTPUT_TSV} ] || [ -f ${OUTPUT_JSON} ]
then
    echo "Found output files. I am about to erase them."
    echo "Abort with ^C if you want to keep them"
    rm --interactive --verbose ${OUTPUT_TSV} ${OUTPUT_JSON}
fi

TMPFILE=$(mktemp)

source ./00-setup.sh

echo "Struct FitType V0 E0 B BP" > ${OUTPUT_TSV}
for element in Al ${elements_all}
do
    struct=Al${element}
    grep $struct $INPUT | awk '{print $NF " " $(NF-1)}' > ${TMPFILE}

    # run fitting script and prepend it with struct info, good for csv
    ./_utils/get_eos_vars.py \
        --datafile ${TMPFILE}  | sed "s/^/$struct /"
done >> ${OUTPUT_TSV} #| sed ':a;N;$!ba;s/RRR\n/ /g'

echo "Space-separated data saved as '${OUTPUT_TSV}'"
echo "------------------"

for element in Al ${elements_all}
do
    struct=Al${element}
    grep $struct $INPUT | awk '{print $NF " " $(NF-1)}' > ${TMPFILE}

    # format the fit info ready to be pasted in a JS code
    ./_utils/get_eos_vars.py \
        --element-name $element \
        --prepare-for-js \
        --datafile ${TMPFILE} | sed "s#// ${element}#// Al${element}#"
done >> ${OUTPUT_JSON}
echo "JS-ready output saved as '${OUTPUT_JSON}'"

rm ${TMPFILE}
